import React, { Component } from "react";
import { dataGlasses } from "./dataGlasses";
export default class BtGlasses extends Component {
  state = {
    glassesList: dataGlasses,
    sanPham: [],
  };
  handleChangeGlasses = (sanPham) => {
    let cloneSanPham = [...this.state.sanPham];
    let index = cloneSanPham.findIndex((item) => {
      return item.id == sanPham.id;
    });
    if (index == -1) {
      let newSanPham = { ...sanPham };
      cloneSanPham.splice(0, 1, newSanPham);
    }
    this.setState({ sanPham: cloneSanPham });
  };
  render() {
    return (
      <div className="bg-light container w-50">
        <div className="my-5 position-relative">
          <img className="w-50 mt-5" src="./glassesImage/model.jpg" alt="" />
          {this.state.sanPham.map((item) => {
            return (
              <div className="position-absolute top-50 start-50 translate-middle d-flex flex-column justify-content-between align-items-center">
                <img className="w-50 opacity-75" src={item.url} alt="" />
                <p className="bg-warning opacity-75 text-center">{item.desc}</p>
              </div>
            );
          })}
        </div>
        <div className="row">
          {this.state.glassesList.map((item) => {
            return (
              <img
                onClick={() => {
                  this.handleChangeGlasses(item);
                }}
                className="btn col-2"
                src={item.url}
                alt=""
              />
            );
          })}
        </div>
      </div>
    );
  }
}
